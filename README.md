[![WrittenIn](https://img.shields.io/badge/Made%20with-HTML%20/%20CSS-1f425f.svg)](https://www.w3.org/html/)
[![CC BY-SA 4.0][cc-by-sa-shield]][cc-by-sa]
[![License](https://img.shields.io/badge/Code%20License-Proprietary-purple)](LICENSE)
[![Preview](https://img.shields.io/badge/Preview-Click%20Here!-blue)](https://thomascat.gitlab.io/miami/)

# Miami ([demo](https://4f77616973.github.io/miami-theme/))

A fast, lightweight, [Vice City / vaporwave](https://entertainment.directv.com/did-grand-theft-auto-vice-city-invent-vaporwave/) inspired template landing page.

## USAGE

Coming soon.

## AUTHORS

* **Owais Shaikh** - *Developer* - GitLab: [ThomasCat](https://gitlab.com/ThomasCat), GitHub: [4f77616973](https://github.com/4f77616973).

## LICENSE

This project is licensed under a Proprietary License - see [LICENSE](LICENSE) for details.

## ACKNOWLEDGEMENT

* I originally made this as a theme for [SlinkyLink](https://slnky.link/), by [Xerfia](https://www.xerfia.com/).

This work is licensed under a
[Creative Commons Attribution-ShareAlike 4.0 International License][cc-by-sa].

[![CC BY-SA 4.0][cc-by-sa-image]][cc-by-sa]

[cc-by-sa]: http://creativecommons.org/licenses/by-sa/4.0/
[cc-by-sa-image]: https://licensebuttons.net/l/by-sa/4.0/88x31.png
[cc-by-sa-shield]: https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg
